/* immutable_collections extension for PHP */

#ifndef PHP_IMMUTABLE_COLLECTIONS_H
# define PHP_IMMUTABLE_COLLECTIONS_H

extern zend_module_entry immutable_collections_module_entry;
# define phpext_immutable_collections_ptr &immutable_collections_module_entry

# define PHP_IMMUTABLE_COLLECTIONS_VERSION "0.1.0"

# if defined(ZTS) && defined(COMPILE_DL_IMMUTABLE_COLLECTIONS)
ZEND_TSRMLS_CACHE_EXTERN()
# endif

#endif	/* PHP_IMMUTABLE_COLLECTIONS_H */

