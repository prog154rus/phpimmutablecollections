--TEST--
Check if immutable_collections is loaded
--SKIPIF--
<?php
if (!extension_loaded('immutable_collections')) {
	echo 'skip';
}
?>
--FILE--
<?php
echo 'The extension "immutable_collections" is available';
?>
--EXPECT--
The extension "immutable_collections" is available
