--TEST--
immutable_collections_test1() Basic test
--SKIPIF--
<?php
if (!extension_loaded('immutable_collections')) {
	echo 'skip';
}
?>
--FILE--
<?php
$ret = immutable_collections_test1();

var_dump($ret);
?>
--EXPECT--
The extension immutable_collections is loaded and working!
NULL
