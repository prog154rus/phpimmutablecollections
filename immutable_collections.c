/* immutable_collections extension for PHP */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "php.h"
#include "ext/standard/info.h"
#include "php_immutable_collections.h"

/* {{{ void immutable_collections_test1()
 */
PHP_FUNCTION(immutable_collections_test1)
{
	ZEND_PARSE_PARAMETERS_NONE();

	php_printf("The extension %s is loaded and working!\r\n", "immutable_collections");
}
/* }}} */

/* {{{ string immutable_collections_test2( [ string $var ] )
 */
PHP_FUNCTION(immutable_collections_test2)
{
	char *var = "World";
	size_t var_len = sizeof("World") - 1;
	zend_string *retval;

	ZEND_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(var, var_len)
	ZEND_PARSE_PARAMETERS_END();

	retval = strpprintf(0, "Hello %s", var);

	RETURN_STR(retval);
}
/* }}}*/

/* {{{ PHP_RINIT_FUNCTION
 */
PHP_RINIT_FUNCTION(immutable_collections)
{
#if defined(ZTS) && defined(COMPILE_DL_IMMUTABLE_COLLECTIONS)
	ZEND_TSRMLS_CACHE_UPDATE();
#endif

	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
 */
PHP_MINFO_FUNCTION(immutable_collections)
{
	php_info_print_table_start();
	php_info_print_table_header(2, "immutable_collections support", "enabled");
	php_info_print_table_end();
}
/* }}} */

/* {{{ arginfo
 */
ZEND_BEGIN_ARG_INFO(arginfo_immutable_collections_test1, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO(arginfo_immutable_collections_test2, 0)
	ZEND_ARG_INFO(0, str)
ZEND_END_ARG_INFO()
/* }}} */

/* {{{ immutable_collections_functions[]
 */
static const zend_function_entry immutable_collections_functions[] = {
	PHP_FE(immutable_collections_test1,		arginfo_immutable_collections_test1)
	PHP_FE(immutable_collections_test2,		arginfo_immutable_collections_test2)
	PHP_FE_END
};
/* }}} */

/* {{{ immutable_collections_module_entry
 */
zend_module_entry immutable_collections_module_entry = {
	STANDARD_MODULE_HEADER,
	"immutable_collections",					/* Extension name */
	immutable_collections_functions,			/* zend_function_entry */
	NULL,							/* PHP_MINIT - Module initialization */
	NULL,							/* PHP_MSHUTDOWN - Module shutdown */
	PHP_RINIT(immutable_collections),			/* PHP_RINIT - Request initialization */
	NULL,							/* PHP_RSHUTDOWN - Request shutdown */
	PHP_MINFO(immutable_collections),			/* PHP_MINFO - Module info */
	PHP_IMMUTABLE_COLLECTIONS_VERSION,		/* Version */
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_IMMUTABLE_COLLECTIONS
# ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
# endif
ZEND_GET_MODULE(immutable_collections)
#endif

